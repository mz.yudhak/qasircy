describe('Qasir Test', () => {
    it('Open Qasir Websites', () => {
        cy.visit('https://web.qasir.id/sign-in')
        cy.get('.heading-12').contains('Masuk ke Dashboard').should('have.text', 'Masuk ke Dashboard')
    })

    it('Login to Qasir',() => {
        cy.get('#sign-in-phonenumber').type('82242455775')
        cy.get('#password').type('641715')
        cy.get('#submit-btn-signin').click()
    })
    
    it('Select Outlet Pusat',() => {
        cy.get('[data-outlet-id="608800"]').click()
    })

    it('Change user profile',() =>{
        cy.get('.user-dropdown').trigger('click')
        cy.contains('Profil Pengguna').click()
        cy.get('[name="first_name"]').clear().type("Ganis")
        cy.get('[name=last_name]').clear().type('Ariffiani')
        cy.get('[name=mobile]').clear().type('082242325642')
        cy.get('[name=email]').clear().type('ganisariffiani@gmail.com')
        cy.get('button').contains('Simpan').click()
        cy.get('.alert-success').should('be.visible')
    })

    it('Relogin and check profile is changed then revert the data',()=>{
        cy.get('.user-dropdown').trigger('click')
        cy.contains('Keluar').click()
        cy.get('#sign-in-phonenumber').type('82242325642')
        cy.get('#password').type('641715')
        cy.get('#submit-btn-signin').click()
        cy.get('[data-outlet-id="608800"]').click()
        cy.get('.user-dropdown').trigger('click')
        cy.contains('Profil Pengguna').click()
        //assert data is changed
        cy.get('[name="first_name"]').should('have.value','Ganis')
        cy.get('[name=last_name]').should('have.value','Ariffiani')
        cy.get('[name=mobile]').should('have.value','082242325642')
        cy.get('[name=email]').should('have.value','ganisariffiani@gmail.com')
        //revert back data
        cy.get('[name="first_name"]').clear().type("Yudha")
        cy.get('[name=last_name]').clear().type('Ferroza')
        cy.get('[name=mobile]').clear().type('082242455775')
        cy.get('[name=email]').clear().type('mz.yudhak@gmail.com')
        cy.get('button').contains('Simpan').click()
    })

    it('Relogin and check profile is revert',()=>{
        cy.get('.user-dropdown').trigger('click')
        cy.contains('Keluar').click()
        cy.get('#sign-in-phonenumber').type('82242455775')
        cy.get('#password').type('641715')
        cy.get('#submit-btn-signin').click()
        cy.get('[data-outlet-id="608800"]').click()
        cy.get('.user-dropdown').trigger('click')
        cy.contains('Profil Pengguna').click()
        //assert data is revert
        cy.get('[name="first_name"]').should('have.value','Yudha')
        cy.get('[name=last_name]').should('have.value','Ferroza')
        cy.get('[name=mobile]').should('have.value','082242455775')
        cy.get('[name=email]').should('have.value','mz.yudhak@gmail.com')
    })
})